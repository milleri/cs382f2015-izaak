PImage img;

void setup() {
  img = loadImage("lab7part2.png");
  size(img.width, img.height);
  noStroke();
  colorMode(HSB, 360, 100, 100);
  image(img,0,0);
  
  loadPixels(); 
  
  for(int i = 0; i < pixels.length; i++){
    float h = hue(pixels[i]);
    float s = saturation(pixels[i]);
    float b = brightness(pixels[i]);
    
  ///water
  if((hue(pixels[i]) > 209) && (hue(pixels[i]) < 216) && (saturation(pixels[i]) > 31) && (saturation(pixels[i]) < 41) && (brightness(pixels[i]) > 99) && (brightness(pixels[i]) < 101)){
    pixels[i] = color(h-5, s+1, b-10);
  } //water
  
  //background
  if((hue(pixels[i]) > 40) && (hue(pixels[i]) < 43) && (saturation(pixels[i]) > 4) && (saturation(pixels[i]) < 6) && (brightness(pixels[i]) > 89) && (brightness(pixels[i]) < 92)){
    pixels[i] = color(h-5, s+5, b+3);
  } //background
  
  //parks
  if((hue(pixels[i]) > 82) && (hue(pixels[i]) < 88) && (saturation(pixels[i]) > 20) && (saturation(pixels[i]) < 28) && (brightness(pixels[i]) > 84) && (brightness(pixels[i]) < 88)){
  pixels[i] = color(h+5, s+5, b+3);
  } //parks
  
    //main routes
  if((hue(pixels[i]) > 33) && (hue(pixels[i]) < 35) && (saturation(pixels[i]) > 84) && (saturation(pixels[i]) < 87) && (brightness(pixels[i]) > 95) && (brightness(pixels[i]) < 100)){
  pixels[i] = color(h+5, s-2, b-7);
  } //main routes
  
    //primary roads
  if((hue(pixels[i]) > -1) && (hue(pixels[i]) < 1) && (saturation(pixels[i]) > -1) && (saturation(pixels[i]) < 1) && (brightness(pixels[i]) > 99) && (brightness(pixels[i]) < 101)){
  pixels[i] = color(h+5, s, b);
  } //primary roads
  
    //side streets
  if((hue(pixels[i]) > 38) && (hue(pixels[i]) < 43) && (saturation(pixels[i]) > 1) && (saturation(pixels[i]) < 5) && (brightness(pixels[i]) > 77) && (brightness(pixels[i]) < 88)){
  pixels[i] = color(h+5, s, b-7);
  } //side streets
  
    //hospitals
  if((hue(pixels[i]) > 5) && (hue(pixels[i]) < 7) && (saturation(pixels[i]) > 10) && (saturation(pixels[i]) < 12) && (brightness(pixels[i]) > 91) && (brightness(pixels[i]) < 93)){
  pixels[i] = color(h, s+5, b-3);
  } //hospitals
  
    //parks
  if((hue(pixels[i]) > 43) && (hue(pixels[i]) < 45) && (saturation(pixels[i]) > 17) && (saturation(pixels[i]) < 19) && (brightness(pixels[i]) > 89) && (brightness(pixels[i]) < 91)){
  pixels[i] = color(h+5, s+2, b-6);
  } //parks
  
  } //for
  updatePixels();
} //setup

void draw(){
 //to update pixel values on mouse click
}

void mouseClicked() {
  loadPixels();
  
  int pos = mouseY*width + mouseX;
  System.out.println("hue: " + hue(pixels[pos]) + " saturation: " + saturation(pixels[pos]) + " brightness: " + brightness(pixels[pos]));
  
} //mouseClicked
