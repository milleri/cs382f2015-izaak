PImage img, img2;

void setup() {
  background(0);
  img = loadImage("lab7part1.png");
  img2 = loadImage("lab7part1.png");
  size(img.width*2, img.height);
  noStroke();
  colorMode(HSB, 360, 100, 100);

  image(img,0,0);
  
  loadPixels(); 
  
  for(int i = 0; i < pixels.length; i++){
    float h = hue(pixels[i]);
    float s = saturation(pixels[i]);
    float b = brightness(pixels[i]);
    
    pixels[i] = color(h, s-35, b);
  }
  updatePixels();
  
  image(img2,img.width,0);
  
} //setup
