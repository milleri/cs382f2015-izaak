

int numPlayers = 15;
int[] points = new int[numPlayers];
void setup() {
  System.out.println("Click on a bar to check the points scored by each player");
  size(730, 400);
  stroke(0);
  colorMode(HSB, 360, 100, 100);
  background(0, 2, 60);
    //You may enter in your own numbers for each players points
    //but i decided to randomly generate numbers between 65 each time
   for(int j = 0; j < numPlayers; j++){
  points[j] = (int)random(0,65);
 } //randomly distributing points

} //set up

void draw() {
  textSize(24);
  fill(0, 0, 0);
  text("Points scored by each player", 150, 20);
   for(int l = 0; l < height; l+=50){
    fill(360, 0, 0);
    line(0, 390 - l, 730, 390 - l);
    textSize(10);
    text(l/5, 0, 390 - l);
  }//for
 int k = 0;
 int value = 20;
 for(int i = 10; i < width && k < numPlayers; i+=(720/numPlayers)) {
    if((i*1.52+value)%360 == 0){
      value = 33;
    }
    fill((i*1.52+value)%360, 70, 90);
    rect(i+10, (400 - points[k]*5) - 10, 720/numPlayers-15, points[k]*5);
    k++;
  } //for

} //draw

void mousePressed(){
  int r = 0;
  int scored;
  for(int i = 10; i < width && r < numPlayers; i+=(720/numPlayers)) {
    if(mouseX > i && mouseX < i+(720/numPlayers)){
      scored = points[r];
      noStroke();
      fill(0,2,60);
      rect(490, 19, 200, 20);
      textSize(15);
      fill(0,0,0);
      text("This player had " + scored + " points", 500, 30);
      stroke(0);

    }
 
    r++;
  } //for


}
