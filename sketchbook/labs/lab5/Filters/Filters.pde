float filter [][][] = {{{-1, -1, -1},             //Edge detection
                        {-1, 8, -1}, 
                        {-1, -1, -1}}};
 
 float myFilter [][][] = {{{0, -1, 0},             //My filter (a version of sharpen)
                          {-1, 5, -1}, 
                          {0, -1, 0}}};                       
PImage img, img2, img3, img4;
int counter = 0;

void setup() {
  img = loadImage("Me.jpg");
  img2 = loadImage("Me.jpg");
  img3 = loadImage("Me.jpg");
  img4 = loadImage("Me.jpg");
  size(img.width*2,img.height*2);
  image(img,0,0);
  image(img2,img.width,0);
  image(img3,0,img.height);
  image(img4,img.width,img.height);
  
  loadPixels();

  
  int size = 2*(img.width * img.height);
  
  for (int i = 0; i < pixels.length; i++) {
    if (((i % (2*img.width)) >= img.width) && (i <= size)) {
    color c = pixels[i];
    float colorRed = red(c);
    float colorGreen = green(c);
    float colorBlue = blue(c);
    pixels[i] = color(255-colorRed, 255-colorGreen, 255-colorBlue);
    } //if
  } //for
  
  updatePixels();
  applyFilter();
  
} //Setup
  
  void applyFilter() {
  for (int y = 0; y < img.height; y++ ) {
    for (int x = 0; x < img.width; x++) {
      img3.pixels[y*img.width+x] = convolution(x, y, filter[counter%filter.length], img);
    } //for
  }//for
  img3.updatePixels();
  image(img3,0,height/2);
  counter++;
  for ( int w = 0; w < img.height; w++){
    for (int z = 0; z < img.width; z++){
     img4.pixels[w*img.width+z] = convolution(z, w, myFilter[counter%myFilter.length], img);
    } //for
  } //for
  img4.updatePixels();
  image(img4,width/2,height/2);
  } //applyFilter
  // calculates the color after applying the filter
color convolution(int x, int y, float[][] matrix, PImage img) {
  int offset = floor(matrix.length/2);
  float r = 0.0, g = 0.0, b = 0.0;

  for (int i = 0; i < matrix.length; i++) {
    for (int j= 0; j < matrix[i].length; j++) {
      // Which neighbor are we using
      int newX = x+i-offset;
      int newY = y+j-offset;
      int idx = img.width*newY + newX;
      // Make sure we haven't walked off our image
      idx = constrain(idx, 0, img.pixels.length-1);
      // Calculate the convolution
      r += (red(img.pixels[idx]) * matrix[i][j]);
      g += (green(img.pixels[idx]) * matrix[i][j]);
      b += (blue(img.pixels[idx]) * matrix[i][j]);
    }
  }
      return color(r,g,b);
}

