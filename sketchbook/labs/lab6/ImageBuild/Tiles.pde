class Tile {
  PImage img;
  int x, y, yStart, xStart,locationX,locationY;
  float angle, spinRate;
  boolean movingX, movingY;
  
  Tile(PImage i, int newX, int newY) {
    img = i;
    x = newX;
    y = newY;
    yStart = newY;
    xStart = newX;
    locationX = (int)random(0,150);
    locationY = (int)random(0,150);
    x = x - locationX;
    y = y - locationY;
    angle = 0;
    spinRate = random(3, 10);
    movingX = true;
    movingY = true;
    
  }//Tile (constructor)
  
  void update() {
    angle = (angle + spinRate) % 360;
    
    if(xStart == x){
    movingX = false;
    }
    if(movingX){
     x++;
    }
    
    if(yStart == y){
      movingY = false;
    }
    if(movingY){
      y++;
    }
  } //update
  
  void drawTile() {
    pushMatrix();
    translate(x + 10, y + 10);
    if (movingX || movingY) {
      rotate(radians(angle));
    } //if
    translate(-10, -10);
    image(img, 0, 0);
    popMatrix();    
  } //drawTile
  
} //Tile (class)
